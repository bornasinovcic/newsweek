# Newsweek Website Project

Welcome to the Newsweek website project repository! This project aims to recreate the Newsweek website, making it responsive and utilizing various technologies including HTML, CSS, JavaScript, PHP, and SQL.

## Table of Contents
- [Features](#features)
- [Technologies](#technologies)
- [Installation](#installation)
- [Usage](#usage)

## Features
1. **Responsive Design**: The website is designed to provide an optimal viewing experience across a wide range of devices, from desktops to mobile phones.
2. **Dynamic Content with PHP and SQL**: Utilizing PHP for server-side scripting and SQL for database interactions, the website fetches and displays dynamic content such as news articles.
3. **Interactive Elements with JavaScript**: Enhance user engagement with interactive elements powered by JavaScript, providing a smooth and enjoyable browsing experience.

## Technologies
- **HTML**: Structure and markup of the website.
- **CSS**: Styling and layout design.
- **JS**: Implementation of interactive features.
- **PHP**: Server-side scripting for dynamic content.
- **SQL**: Database interactions for storing and retrieving data.

## Installation
1. Clone the repository to your local machine:
    ```bash
    git clone https://gitlab.com/bornasinovcic/newsweek.git
    ```
2. Set up your web server environment with PHP and MySQL support.
3. Import the provided SQL file (database.sql) to set up the necessary database structure.
4. Configure your database connection in the PHP files where needed.
5. Open the project in your preferred code editor to explore the code.

## Usage
1. Navigate to the project root directory in your web browser.
2. Explore the responsive design by resizing your browser window or using different devices.
3. Interact with the dynamic content by browsing through news articles.
4. Test any additional features or functionalities you've implemented.
